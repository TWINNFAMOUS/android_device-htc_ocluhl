#!/sbin/sh

SCRIPTNAME="PatchLevel"
LOGFILE=/tmp/recovery.log
TEMPSYS=/s
TEMPVEN=/v
BUILDPROP=system/build.prop
DEFAULTPROP=prop.default
suffix=$(getprop ro.boot.slot_suffix)

if [ -z "$suffix" ]; then
	suf=$(getprop ro.boot.slot)
	suffix="_$suf"
fi

venpath="/dev/block/bootdevice/by-name/vendor$suffix"
syspath="/dev/block/bootdevice/by-name/system$suffix"

log_info()
{
	echo "I:$SCRIPTNAME:"$1 >> $LOGFILE
}

log_error()
{
	echo "E:$SCRIPTNAME:"$1 >> $LOGFILE
}

finish()
{
	umount "$TEMPVEN"
	umount "$TEMPSYS"
	rmdir "$TEMPVEN"
	rmdir "$TEMPSYS"
	setprop crypto.ready 1
	log_info "crypto.ready=$(getprop crypto.ready)"
	log_info "Script complete. Device ready for decryption."
	exit 0
}

finish_error()
{
	umount "$TEMPVEN"
	umount "$TEMPSYS"
	rmdir "$TEMPVEN"
	rmdir "$TEMPSYS"
	log_error "Script run incomplete. Device not ready for decryption."
	exit 0
}

osver_orig=$(getprop ro.build.version.release_orig)
patchlevel_orig=$(getprop ro.build.version.security_patch_orig)
osver=$(getprop ro.build.version.release)
patchlevel=$(getprop ro.build.version.security_patch)
device=$(getprop ro.product.device)
fingerprint=$(getprop ro.build.fingerprint)
product=$(getprop ro.build.product)

log_info "Running patchlevel pre-decrypt script for TWRP..."

mkdir "$TEMPVEN"
if [ -d "$TEMPVEN" ]; then
	log_info "Temporary vendor folder created at $TEMPVEN."
else
	log_error "Unable to create temporary vendor folder."
	finish_error
fi
mount -t ext4 -o ro "$venpath" "$TEMPVEN"
if [ -n "$(ls -A "$TEMPVEN" 2>/dev/null)" ]; then
	log_info "Vendor mounted at $TEMPVEN."
else
	log_error "Unable to mount vendor to temporary folder."
	finish_error
fi

mkdir "$TEMPSYS"
if [ -d "$TEMPSYS" ]; then
	log_info "Temporary system folder created at $TEMPSYS."
else
	log_error "Unable to create temporary system folder."
	finish_error
fi
mount -t ext4 -o ro "$syspath" "$TEMPSYS"
if [ -n "$(ls -A "$TEMPSYS" 2>/dev/null)" ]; then
	log_info "System mounted at $TEMPSYS."
else
	log_error "Unable to mount system to temporary folder."
	finish_error
fi

if [ -f "$TEMPSYS/$BUILDPROP" ]; then
	log_info "Build.prop exists! Setting system properties from build.prop"
	# TODO: It may be better to try to read these from the boot image than from /system
	log_info "Current OS version: $osver"
	osver=$(grep -i 'ro.build.version.release' "$TEMPSYS/$BUILDPROP"  | cut -f2 -d'=' -s)
	if [ -n "$osver" ]; then
		resetprop ro.build.version.release "$osver"
		sed -i "s/ro.build.version.release=.*/ro.build.version.release="$osver"/g" /$DEFAULTPROP ;
		log_info "New OS Version: $osver"
	fi
	log_info "Current security patch level: $patchlevel"
	patchlevel=$(grep -i 'ro.build.version.security_patch' "$TEMPSYS/$BUILDPROP"  | cut -f2 -d'=' -s)
	if [ -n "$patchlevel" ]; then
		resetprop ro.build.version.security_patch "$patchlevel"
		sed -i "s/ro.build.version.security_patch=.*/ro.build.version.security_patch="$patchlevel"/g" /$DEFAULTPROP ;
		log_info "New security patch level: $patchlevel"
	fi
	# Set additional props from build.prop
	# Only needed for some devices, so this section can be removed if your device isn't one of them
	log_info "Current device: $device"
	device=$(grep -i 'ro.product.device' "$TEMPSYS/$BUILDPROP"  | cut -f2 -d'=' -s)
	if [ -n "$device" ]; then
		resetprop ro.product.device "$device"
		sed -i "s/ro.product.device=.*/ro.product.device="$device"/g" /$DEFAULTPROP ;
		log_info "New device: $device"
	fi
	log_info "Current fingerprint: $fingerprint"
	fingerprint=$(grep -i 'ro.build.fingerprint' "$TEMPSYS/$BUILDPROP"  | cut -f2 -d'=' -s)
	if [ -n "$fingerprint" ]; then
		resetprop ro.build.fingerprint "$fingerprint"
		sed -i "s/ro.build.fingerprint=.*/ro.build.fingerprint="$fingerprint"/g" /$DEFAULTPROP ;
		log_info "New fingerprint: $fingerprint"
	fi
	log_info "Current product: $product"
	product=$(grep -i 'ro.build.product' "$TEMPSYS/$BUILDPROP"  | cut -f2 -d'=' -s)
	if [ -n "$product" ]; then
		resetprop ro.build.product "$product"
		sed -i "s/ro.build.product=.*/ro.build.product="$product"/g" /$DEFAULTPROP ;
		log_info "New product: $product"
	fi
	# Load Tuxera exfat module
	if [ -f "$TEMPVEN/lib/modules/texfat.ko" ]; then
		echo "I:TexFAT:Loading Tuxera exFAT module from vendor..." >> $LOGFILE
		insmod "$TEMPVEN/lib/modules/texfat.ko"
	fi
	finish
else
	# Be sure to increase the PLATFORM_VERSION in build/core/version_defaults.mk to override Google's anti-rollback features to something rather insane
	if [ -n "$osver_orig" ]; then
		log_info "Original OS version: $osver_orig"
		log_info "Current OS version: $osver"
		log_info "Setting OS Version to $osver_orig"
		osver=$osver_orig
		resetprop ro.build.version.release "$osver"
		sed -i "s/ro.build.version.release=.*/ro.build.version.release="$osver"/g" /$DEFAULTPROP ;
	else
		log_info "No Original OS Version found. Proceeding with existing value."
		log_info "Current OS version: $osver"
	fi
	if [ -n "$patchlevel_orig" ]; then
		log_info "Original security patch level: $patchlevel_orig"
		log_info "Current security patch level: $patchlevel"
		log_info "Setting security patch level to $patchlevel_orig"
		patchlevel=$patchlevel_orig
		resetprop ro.build.version.security_patch "$patchlevel"
		sed -i "s/ro.build.version.security_patch=.*/ro.build.version.security_patch="$patchlevel"/g" /$DEFAULTPROP ;
	else
		log_info "No Original security patch level found. Proceeding with existing value."
		log_info "Current security patch level: $patchlevel"
	fi
	# Load Tuxera exfat module
	if [ -f "$TEMPVEN/lib/modules/texfat.ko" ]; then
		echo "I:TexFAT:Loading Tuxera exFAT module from vendor..." >> $LOGFILE
		insmod "$TEMPVEN/lib/modules/texfat.ko"
	fi
	finish
fi

###### NOTE: The below is no longer used but I'm keeping it here in case it is needed again at some point!
mkdir -p /vendor/lib64/hw/

cp /s/system/lib64/android.hidl.base@1.0.so /sbin/
cp /s/system/lib64/libicuuc.so /sbin/
cp /s/system/lib64/libxml2.so /sbin/

relink /v/bin/qseecomd

cp /v/lib64/libdiag.so /vendor/lib64/
cp /v/lib64/libdrmfs.so /vendor/lib64/
cp /v/lib64/libdrmtime.so /vendor/lib64/
cp /v/lib64/libGPreqcancel.so /vendor/lib64/
cp /v/lib64/libGPreqcancel_svc.so /vendor/lib64/
cp /v/lib64/libqdutils.so /vendor/lib64/
cp /v/lib64/libqisl.so /vendor/lib64/
cp /v/lib64/libqservice.so /vendor/lib64/
cp /v/lib64/libQSEEComAPI.so /vendor/lib64/
cp /v/lib64/librecovery_updater_msm.so /vendor/lib64/
cp /v/lib64/librpmb.so /vendor/lib64/
cp /v/lib64/libsecureui.so /vendor/lib64/
cp /v/lib64/libSecureUILib.so /vendor/lib64/
cp /v/lib64/libsecureui_svcsock.so /vendor/lib64/
cp /v/lib64/libspcom.so /vendor/lib64/
cp /v/lib64/libspl.so /vendor/lib64/
cp /v/lib64/libssd.so /vendor/lib64/
cp /v/lib64/libStDrvInt.so /vendor/lib64/
cp /v/lib64/libtime_genoff.so /vendor/lib64/
cp /v/lib64/libkeymasterdeviceutils.so /vendor/lib64/
cp /v/lib64/libkeymasterprovision.so /vendor/lib64/
cp /v/lib64/libkeymasterutils.so /vendor/lib64/
cp /v/lib64/libqtikeymaster4.so /vendor/lib64/
cp /v/lib64/vendor.qti.hardware.tui_comm@1.0.so /vendor/lib64/
cp /v/lib64/hw/bootctrl.sdm660.so /vendor/lib64/hw/
cp /v/lib64/hw/android.hardware.boot@1.0-impl.so /vendor/lib64/hw/
cp /v/lib64/hw/android.hardware.gatekeeper@1.0-impl-qti.so /vendor/lib64/hw/

cp /v/manifest.xml /vendor/
cp /v/compatibility_matrix.xml /vendor/

relink /v/bin/hw/android.hardware.boot@1.0-service
relink /v/bin/hw/android.hardware.gatekeeper@1.0-service-qti
relink /v/bin/hw/android.hardware.keymaster@3.0-service-qti

finish
exit 0
